
import React , {Component} from 'react'

class PageTwo extends Component {
    render() {
        return(
        <div className='row container'>
            <ul className='collection'>
                <li className='collection-item avatar'>
                    <img src='images/yuna.jpg' alt='' className='circle'/>
                    <span className='title'>Title</span>
                    <p>First Line <br/>
                        Second Line
                    </p>
                    <a href='#!' className='secondary-content'><i className='material-icons'>grade</i></a>
                </li>
                <li className='collection-item avatar'>
                    <i className='material-icons circle'>folder</i>
                    <span className='title'>Title</span>
                    <p>First Line <br/>
                        Second Line
                    </p>
                    <a href='#!' className='secondary-content'><i className='material-icons'>grade</i></a>
                </li>
                <li className='collection-item avatar'>
                    <i className='material-icons circle green'>insert_chart</i>
                    <span className='title'>Title</span>
                    <p>First Line <br/>
                        Second Line
                    </p>
                    <a href='#!' className='secondary-content'><i className='material-icons'>grade</i></a>
                </li>
                <li className='collection-item avatar'>
                    <i className='material-icons circle red'>play_arrow</i>
                    <span className='title'>Title</span>
                    <p>First Line <br/>
                        Second Line
                    </p>
                    <a href='#!' className='secondary-content'><i className='material-icons'>grade</i></a>
                </li>
            </ul>
            <button type='submit' className='btn waves-effect waves-light' onClick={() => this.props.changePage(1)}>
                <a href='#/' style={{color: 'white'}}>accueil</a>
            </button>
        </div>
        )
    }
}

export default PageTwo

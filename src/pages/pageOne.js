
import React , {Component} from 'react'

class PageOne extends Component {
    render() {
        return(
        <div className='container'>
            <div className='row'>
                <form className='col s12'>
                    <div className='row'>
                        <div className='input-field col s6'>
                            <input id='first_name' type='text' className='validate'/>
                            <label htmlFor='first_name'>First Name</label>
                        </div>
                        <div className='input-field col s6'>
                            <input id='last_name' type='text' className='validate'/>
                            <label htmlFor='last_name'>Last Name</label>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='input-field col s12'>
                            <input id='password' type='password' className='validate'/>
                            <label htmlFor='password'>Password</label>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='input-field col s12'>
                            <input id='email' type='email' className='validate'/>
                            <label htmlFor='email'>Email</label>
                        </div>
                    </div>
                </form>
                <button type='submit' className='btn waves-effect waves-light' onClick={() => this.props.changePage(2)}>
                    <a href='#/one' style={{color: 'white'}}>OK</a>
                </button>
            </div>
        </div>
        )
    }
}

export default PageOne

import React, { Component } from 'react'
import './App.css'
import PageOne from './pages/pageOne'
import PageTwo from './pages/pageTwo'
//import socket from 'socket.io-client'

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            pageIndex: 1
        }
        this.toOtherPage = this.toOtherPage.bind(this)
    }

    handlePageChange() {
        switch (this.state.pageIndex) {
            case 1:
                return(
                    <PageOne changePage={this.toOtherPage}/>
                )
            case 2:
                return(
                    <PageTwo changePage={this.toOtherPage}/>
                )
            default :
                return(
                    <div>
                        <p>coucou</p>
                    </div>
                )
        }
    }

  toOtherPage(pageIndex) {
      this.setState( { pageIndex: pageIndex } )
  }

  render() {
      return (
          <div>
              {this.handlePageChange()}
          </div>
      )
  }
}

export default App
